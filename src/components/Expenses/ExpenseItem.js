import styles from "./ExpenseItem.module.css";

const ExpenseItem = (props) => {
  return (
    <div className={styles.expense__item}>
      <div className={styles.expense__date}>
        <p>
          {props.expenseItem.date.toLocaleString("en-US", { month: "long" })}
        </p>
        <p>{props.expenseItem.date.getFullYear()}</p>
        <p>{props.expenseItem.date.getDate()}</p>
      </div>
      <div className={styles.expense__title}>
        <p>{props.expenseItem.title}</p>
      </div>
      <div className={styles.expense__amount}>
        <p>${props.expenseItem.amount}</p>
      </div>
    </div>
  );
};

export default ExpenseItem;
