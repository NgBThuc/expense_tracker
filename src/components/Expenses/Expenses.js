import Chart from "../Chart/Chart";
import Container from "../UI/Container";
import ExpensesFilter from "./ExpensesFilter";
import ExpensesList from "./ExpensesList";

const Expenses = (props) => {
  return (
    <Container style={{ backgroundColor: "#1f1f1f" }}>
      <ExpensesFilter yearFilterHandler={props.yearFilterHandler} />
      <Chart filterExpensesData={props.filterExpensesData} />
      <ExpensesList filterExpensesData={props.filterExpensesData} />
    </Container>
  );
};

export default Expenses;
