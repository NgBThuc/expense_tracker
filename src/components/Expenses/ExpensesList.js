import ExpenseItem from "./ExpenseItem";
import styles from "./ExpensesList.module.css";

const ExpensesList = (props) => {
  if (props.filterExpensesData.length === 0) {
    return (
      <h2 style={{ textAlign: "center" }}>
        You dont have any expense in this year
      </h2>
    );
  }

  return (
    <div className={styles.expenses__list}>
      {props.filterExpensesData.map((expenseItem) => (
        <ExpenseItem key={expenseItem.id} expenseItem={expenseItem} />
      ))}
    </div>
  );
};

export default ExpensesList;
