import styles from "./ExpensesFilter.module.css";

const ExpensesFilter = (props) => {
  const yearSelectHandler = (event) => {
    props.yearFilterHandler(event.target.value);
  };

  return (
    <div className={styles.filter}>
      <label className={styles.filter__label}>Filter by year</label>
      <select
        onChange={yearSelectHandler}
        id="filterList"
        className={styles.filter__list}
      >
        <option value="2022">2022</option>
        <option value="2021">2021</option>
        <option value="2020">2020</option>
        <option value="2019">2019</option>
      </select>
    </div>
  );
};

export default ExpensesFilter;
