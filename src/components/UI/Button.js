import styles from "./Button.module.css";

const Button = (props) => {
  return (
    <button
      onClick={props.onClick}
      style={props.style}
      className={styles.btn}
      type={props.type}
    >
      {props.children}
    </button>
  );
};

export default Button;
