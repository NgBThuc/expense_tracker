import { useState } from "react";
import Button from "../UI/Button";
import Container from "../UI/Container";
import FormGroup from "./FormGroup";
import styles from "./NewExpense.module.css";

const NewExpense = (props) => {
  let [expenseItem, setExpenseItem] = useState({
    title: "",
    amount: "",
    date: "",
  });
  let [isEdit, setIsEdit] = useState(false);

  const onInputChange = (label, value) => {
    setExpenseItem((prevExpenseItem) => {
      return { ...prevExpenseItem, [label]: value };
    });
  };

  const formSubmitHandler = (event) => {
    hideFormHandler();
    event.preventDefault();
    props.onFormSubmit(expenseItem);
  };

  const showFormHandler = () => {
    setIsEdit(true);
  };

  const hideFormHandler = () => {
    setIsEdit(false);
  };

  if (!isEdit) {
    return (
      <Container style={{ backgroundColor: "#a892ee" }}>
        <Button
          type="button"
          onClick={showFormHandler}
          style={{ display: "block", marginInline: "auto" }}
        >
          New Product
        </Button>
      </Container>
    );
  }

  return (
    <Container style={{ backgroundColor: "#a892ee" }}>
      <form onSubmit={formSubmitHandler}>
        <FormGroup type="text" label="Title" onInputChange={onInputChange} />
        <FormGroup type="text" label="Amount" onInputChange={onInputChange} />
        <FormGroup type="date" label="Date" onInputChange={onInputChange} />
        <div className={styles.form__btn}>
          <Button onClick={hideFormHandler} type="button">
            Cancel
          </Button>
          <Button type="submit">Add Expense</Button>
        </div>
      </form>
    </Container>
  );
};

export default NewExpense;
