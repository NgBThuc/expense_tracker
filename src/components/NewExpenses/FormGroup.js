import styles from "./FormGroup.module.css";

const FormGroup = (props) => {
  const parseDate = (dateValue) => {
    let dateSplit = dateValue.split(/\D/);
    let year = dateSplit[0];
    let month = dateSplit[1] - 1;
    let day = dateSplit[2];
    return new Date(year, month, day);
  };

  const inputChangeHandler = (event, label) => {
    if (props.type === "date") {
      props.onInputChange(label, parseDate(event.target.value));
      return;
    }
    props.onInputChange(label, event.target.value);
  };

  return (
    <div className={styles.form__group}>
      <label htmlFor={props.label.toLowerCase()}>{props.label}</label>
      <input
        onChange={(event) =>
          inputChangeHandler(event, props.label.toLowerCase())
        }
        id={props.label.toLowerCase()}
        type={props.type}
      />
    </div>
  );
};

export default FormGroup;
