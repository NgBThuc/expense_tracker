import styles from "./ChartBar.module.css";

const ChartBar = (props) => {
  let chartBarHeight = "0%";

  if (props.max) {
    chartBarHeight = Math.round((props.value / props.max) * 100) + "%";
  }

  return (
    <div className={styles.chart__bar}>
      <div className={styles.chart__inner}>
        <div
          style={{ height: chartBarHeight }}
          className={styles.chart__fill}
        ></div>
      </div>
      <p>{props.month}</p>
    </div>
  );
};

export default ChartBar;
