import { nanoid } from "nanoid";
import styles from "./Chart.module.css";
import ChartBar from "./ChartBar";

const Chart = (props) => {
  const chartData = [
    {
      id: nanoid(),
      month: "Jan",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Feb",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Mar",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Apr",
      value: 0,
    },
    {
      id: nanoid(),
      month: "May",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Jun",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Jul",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Aug",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Sep",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Oct",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Nov",
      value: 0,
    },
    {
      id: nanoid(),
      month: "Dec",
      value: 0,
    },
  ];

  for (const expense of props.filterExpensesData) {
    let month = expense.date.getMonth();
    chartData[month].value += expense.amount;
  }

  let max = Math.max(
    ...chartData.map((chartDataPoint) => chartDataPoint.value)
  );

  return (
    <div className={styles.chart}>
      {chartData.map((chartDataPoint) => {
        return (
          <ChartBar
            key={chartDataPoint.id}
            month={chartDataPoint.month}
            value={chartDataPoint.value}
            max={max}
          />
        );
      })}
    </div>
  );
};

export default Chart;
