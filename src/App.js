import { nanoid } from "nanoid";
import { useState } from "react";
import "./App.css";
import Expenses from "./components/Expenses/Expenses";
import NewExpense from "./components/NewExpenses/NewExpense";

let initialData = [
  {
    id: nanoid(),
    title: "Toilet Paper",
    amount: 94.12,
    date: new Date(2020, 7, 14),
  },
  {
    id: nanoid(),
    title: "New TV",
    amount: 799.49,
    date: new Date(2021, 2, 12),
  },
  {
    id: nanoid(),
    title: "Car Insurance",
    amount: 294.67,
    date: new Date(2021, 2, 28),
  },
  {
    id: nanoid(),
    title: "New Desk (Wooden)",
    amount: 450,
    date: new Date(2021, 5, 12),
  },
];

const App = () => {
  const [expensesData, setExpensesData] = useState(initialData);
  const [filterYear, setFilterYear] = useState("2022");

  const onFormSubmit = (expenseItem) => {
    let expenseItemWithID = { ...expenseItem, id: nanoid() };
    setExpensesData((prevExpensesData) => {
      return [expenseItemWithID, ...prevExpensesData];
    });
  };

  const yearFilterHandler = (selectedYear) => {
    setFilterYear(selectedYear);
  };

  const filterExpensesData = expensesData.filter(
    (expense) => expense.date.getFullYear().toString() === filterYear
  );

  return (
    <div>
      <NewExpense onFormSubmit={onFormSubmit} />
      <Expenses
        yearFilterHandler={yearFilterHandler}
        filterExpensesData={filterExpensesData}
      />
    </div>
  );
};

export default App;
